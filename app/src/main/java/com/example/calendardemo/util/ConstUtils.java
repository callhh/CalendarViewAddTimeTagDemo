package com.example.calendardemo.util;

/**
 * Created by callhh on 2019/12/6
 * 常量工具类
 */
public class ConstUtils {
    /**
     * 星期栏
     * 0-6 == 周日-周六
     */
    public static final int KEY_ITEM_0 = 0;
    public static final int KEY_ITEM_1 = 1;
    public static final int KEY_ITEM_2 = 2;
    public static final int KEY_ITEM_3 = 3;
    public static final int KEY_ITEM_4 = 4;
    public static final int KEY_ITEM_5 = 5;
    public static final int KEY_ITEM_6 = 6;
    public static final int KEY_ITEM_7 = 7;
    public static final String TYPE_ADD = "ADD";
    public static final String TYPE_DELETE = "DELETE";


}
