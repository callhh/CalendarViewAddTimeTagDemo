package com.example.calendardemo.beans;

/**
 * 添加时间段标签Bean
 */
public class TimeTagBeans {

    public TimeTagBeans(String st_time_one, String st_time_two, String end_time_one, String end_time_two) {
        this.st_time_one = st_time_one;
        this.st_time_two = st_time_two;
        this.end_time_one = end_time_one;
        this.end_time_two = end_time_two;
    }

    private String st_time_one;
   private String st_time_two;
   private String end_time_one;
   private String end_time_two;

    public String getSt_time_one() {
        return st_time_one == null ? "" : st_time_one;
    }

    public void setSt_time_one(String st_time_one) {
        this.st_time_one = st_time_one;
    }

    public String getSt_time_two() {
        return st_time_two == null ? "" : st_time_two;
    }

    public void setSt_time_two(String st_time_two) {
        this.st_time_two = st_time_two;
    }

    public String getEnd_time_one() {
        return end_time_one == null ? "" : end_time_one;
    }

    public void setEnd_time_one(String end_time_one) {
        this.end_time_one = end_time_one;
    }

    public String getEnd_time_two() {
        return end_time_two == null ? "" : end_time_two;
    }

    public void setEnd_time_two(String end_time_two) {
        this.end_time_two = end_time_two;
    }
}
