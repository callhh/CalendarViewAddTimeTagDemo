package com.example.calendardemo.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.calendardemo.R;
import com.example.calendardemo.beans.TimeTagBeans;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private List<TimeTagBeans> timeNoteBeans;
    private LayoutInflater inflater;

    public MyAdapter(Context context, List<TimeTagBeans> beans) {
        super();
        this.timeNoteBeans = beans;
        this.context = context;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return timeNoteBeans == null ? 0 : timeNoteBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_item_cycle, parent, false);
            holder.edt_one = view.findViewById(R.id.edt_start_time_one);
            holder.edt_two = view.findViewById(R.id.edt_start_time_two);
            holder.edt_three = view.findViewById(R.id.edt_end_time_one);
            holder.edt_four = view.findViewById(R.id.edt_end_time_two);
            holder.tv_del = view.findViewById(R.id.del);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        //清除焦点
        holder.edt_one.clearFocus();
        holder.edt_two.clearFocus();
        holder.edt_three.clearFocus();
        holder.edt_four.clearFocus();

        //先清除之前的文本改变监听
        if (holder.edt_one.getTag() instanceof TextWatcher){
            holder.edt_one.removeTextChangedListener((TextWatcher) holder.edt_one.getTag());
        }
        //先清除之前的文本改变监听
        if (holder.edt_two.getTag() instanceof TextWatcher){
            holder.edt_two.removeTextChangedListener((TextWatcher) holder.edt_two.getTag());
        }

        //先清除之前的文本改变监听
        if (holder.edt_three.getTag() instanceof TextWatcher){
            holder.edt_three.removeTextChangedListener((TextWatcher) holder.edt_three.getTag());
        }
        //先清除之前的文本改变监听
        if (holder.edt_four.getTag() instanceof TextWatcher){
            holder.edt_four.removeTextChangedListener((TextWatcher) holder.edt_four.getTag());
        }

        //设置数据
        holder.edt_one.setText(TextUtils.isEmpty(timeNoteBeans.get(position).getSt_time_one()) ?
                "" : timeNoteBeans.get(position).getSt_time_one());
        holder.edt_two.setText(TextUtils.isEmpty(timeNoteBeans.get(position).getSt_time_two()) ?
                "" : timeNoteBeans.get(position).getSt_time_two());
        holder.edt_three.setText(TextUtils.isEmpty(timeNoteBeans.get(position).getEnd_time_one()) ?
                "" : timeNoteBeans.get(position).getEnd_time_one());
        holder.edt_four.setText(TextUtils.isEmpty(timeNoteBeans.get(position).getEnd_time_two()) ?
                "" : timeNoteBeans.get(position).getEnd_time_two());

        //文本改变监听
        final TextWatcher oneEitext = new SimpeTextWather() {
            @Override
            public void afterTextChanged(Editable s) {
                    if(TextUtils.isEmpty(s)){
                        timeNoteBeans.get(position).setSt_time_one("");
                    }else{
                        timeNoteBeans.get(position).setSt_time_one(String.valueOf(s));
                    }
            }
        };
        final TextWatcher twoEitext = new SimpeTextWather() {
            @Override
            public void afterTextChanged(Editable s) {
                if(TextUtils.isEmpty(s)){
                    timeNoteBeans.get(position).setSt_time_two("");
                }else{
                    timeNoteBeans.get(position).setSt_time_two(String.valueOf(s));
                }
            }
        };
        final TextWatcher threeEitext = new SimpeTextWather() {
            @Override
            public void afterTextChanged(Editable s) {
                if(TextUtils.isEmpty(s)){
                    timeNoteBeans.get(position).setEnd_time_one("");
                }else{
                    timeNoteBeans.get(position).setEnd_time_one(String.valueOf(s));
                }
            }
        };
        final TextWatcher fourEitext = new SimpeTextWather() {
            @Override
            public void afterTextChanged(Editable s) {
                if(TextUtils.isEmpty(s)){
                    timeNoteBeans.get(position).setEnd_time_two("");
                }else{
                    timeNoteBeans.get(position).setEnd_time_two(String.valueOf(s));
                }
            }
        };

        //把监听设置到不同的Editext上
        holder.edt_one.addTextChangedListener(oneEitext);
        holder.edt_two.addTextChangedListener(twoEitext);
        holder.edt_three.addTextChangedListener(threeEitext);
        holder.edt_four.addTextChangedListener(fourEitext);

        holder.edt_one.setTag(oneEitext);
        holder.edt_two.setTag(twoEitext);
        holder.edt_three.setTag(threeEitext);
        holder.edt_four.setTag(fourEitext);


        holder.tv_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (onDelClickListener != null){
                        onDelClickListener.del(v,position);
                    }
            }
        });

        return view;
    }

    //刷新列表
    public void refreshData(List<TimeTagBeans> list){
        timeNoteBeans = list;
        notifyDataSetChanged();
    }



    public abstract class SimpeTextWather implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }


    private static class ViewHolder {
        private EditText edt_one,edt_two,edt_three,edt_four;
        private TextView tv_del;
    }

    public interface onDelClickListener {
         void del(View v,int pos);
    }

    private onDelClickListener onDelClickListener;

    public void setOnDayClickListener(onDelClickListener listener){
        this.onDelClickListener = listener;
    }
}