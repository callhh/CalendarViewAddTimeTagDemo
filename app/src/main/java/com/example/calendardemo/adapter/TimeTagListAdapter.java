package com.example.calendardemo.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calendardemo.R;
import com.example.calendardemo.beans.TimeTagBeans;

import java.util.ArrayList;
import java.util.List;

/**
 * 时间段标签Adapter
 */
public class TimeTagListAdapter extends BaseAdapter {

    private Context context;
    private List<TimeTagBeans> timeNoteBeans = new ArrayList<>();
    private LayoutInflater inflater;

    public TimeTagListAdapter(Context context, List<TimeTagBeans> beans) {
        super();
        this.timeNoteBeans = beans;
        this.context = context;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return timeNoteBeans == null ? 0 : timeNoteBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_item_time_tag, parent, false);
            holder.tvStartTime = view.findViewById(R.id.tvStartTime);
            holder.tvEndTime = view.findViewById(R.id.tvEndTime);
            holder.llContentLayout = view.findViewById(R.id.llContentLayout);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        try {
            TimeTagBeans timeTagBeans = timeNoteBeans.get(position);
            String st_time_one = timeTagBeans.getSt_time_one();
            String st_time_two = timeTagBeans.getSt_time_two();
            String end_time_one = timeTagBeans.getEnd_time_one();
            String end_time_two = timeTagBeans.getEnd_time_two();
            if (TextUtils.isEmpty(st_time_one) ||TextUtils.isEmpty(st_time_two)
                    || TextUtils.isEmpty(end_time_one) || TextUtils.isEmpty(end_time_two)) {
                holder.llContentLayout.setVisibility(View.GONE);
                holder.tvStartTime.setText(st_time_one + ":" + st_time_two);
                holder.tvEndTime.setText(end_time_one + ":" + end_time_two);

            }else {
                holder.llContentLayout.setVisibility(View.VISIBLE);
                String startTime1 = setNumberDisplayStandard(Integer.parseInt(st_time_one));
                String startTime2 = setNumberDisplayStandard(Integer.parseInt(st_time_two));
                String endTime1 = setNumberDisplayStandard(Integer.parseInt(end_time_one));
                String endTime2 = setNumberDisplayStandard(Integer.parseInt(end_time_two));
                holder.tvStartTime.setText(startTime1 + ":" + startTime2);
                holder.tvEndTime.setText(endTime1 + ":" + endTime2);
            }

        } catch (Exception e) {
            Log.i("callhh", e.toString());
        }
        return view;
    }

    /**
     * 设置时间格式显示标准
     *
     * @param num 时间数字
     * @return 返回时间字符串
     */
    private String setNumberDisplayStandard(int num) {
        String numStr;
        if (num < 10) {
            numStr = "0" + num;
        } else {
            numStr = String.valueOf(num);
        }
        return numStr;
    }

    //刷新列表
    public void refreshData(List<TimeTagBeans> list) {
        timeNoteBeans = list;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        private TextView tvStartTime, tvEndTime;
        private LinearLayout llContentLayout;
    }

    public interface onDelClickListener {
        void del(View v, int pos);
    }

    private onDelClickListener onDelClickListener;

    public void setOnDayClickListener(onDelClickListener listener) {
        this.onDelClickListener = listener;
    }
}